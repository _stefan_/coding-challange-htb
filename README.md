
# Coding Challenge

## Installation

Create a virtual environment (needs to be done once)

``` bash
virtualenv venv
```

Activate virtual environment (needs to be done every time when starting to work on this project)

``` bash
source venv/bin/activate
```

Install requirements (needs to be done initially or when a package has been added)

``` bash
make init
```

## Development

When coding the server can be started in debug mode for auto refresh and interactive debugging.

``` bash
make run-dev
```

## Test

### Unit Tests

The unit tests can be started the following way:

``` bash
make test-unit
```

### Functional Tests

For functional tests the server needs to be started with `make run-dev` or `make run`.

With the functional test API-calls are performed.

``` bash
make test-functional
```

## Build

Build the docker image.

``` bash
make build
```

## Run

After building the image the server can be started.

``` bash
make run
```
