
init:
	@echo "Install requirements"
	pip install -r requirements.txt

test-unit:
	@echo "Run unit tests"
	pytest tests/.

test-functional:
	@echo "Run functional tests"
	./tests/functional/test_rate.sh

run-dev:
	@echo "Run app in debug mode (auto reload, interactive debugger)"
	flask --app csms --debug run

build:
	@echo "Build image"
	docker build --tag coding-challenge:latest .

run:
	@echo "Run app"
	docker run --detach --publish 5000:5000 coding-challenge
