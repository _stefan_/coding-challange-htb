
from flask import request
from marshmallow import (
    fields,
    Schema,
    ValidationError,
)

from csms import (
    app,
    transactions_applier,
)

class RateSchema(Schema):
    energy = fields.Float(required=True)
    time = fields.Float(required=True)
    transaction = fields.Float(required=True)

class CDRSchema(Schema):
    meterStart = fields.Int(required=True)
    timestampStart = fields.String(required=True)
    meterStop = fields.Int(required=True)
    timestampStop = fields.String(required=True)

class TransactionSchema(Schema):
    rate = fields.Nested(RateSchema, required=True)
    cdr = fields.Nested(CDRSchema, required=True)

transaction_schema =  TransactionSchema()


@app.route("/rate", methods=['POST'])
def rate():
    """Calculate the overall cost and the costs of the single components with
    the provided rate and the charge detail record.

    :return: Overall cost and cost of the single components
    :rtype: dict
    """
    json_data = request.get_json()
    if not json_data:
        return {"message": "No input data provided"}, 400
    try:
        data = transaction_schema.load(json_data)
    except ValidationError as err:
        return err.messages, 422

    return transactions_applier.rating(json_data["rate"], json_data["cdr"])
