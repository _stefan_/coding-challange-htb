
from dateutil import parser
from decimal import Decimal


class Rate(object):
    def __init__(self, energy, time, transaction):
        """Initialize class

        :param energy: Cost per kWh
        :type energy: float
        :param time: Cost per hour
        :type time: float
        :param transaction: Cost per transaction
        :type transaction: float
        """
        self.energy_cost_per_unit = energy
        self.time_cost_per_unit = time
        self.transaction_cost = transaction

    def calculate(self, duration, charged_energy):
        """The actual calculation of the rating

        :param duration: Charging time in seconds
        :type duration: int
        :param charged_energy: Energy which got charged in Wh
        :type charged_energy: int
        :return: Overall cost and cost of the single components
        :rtype: dict
        """
        energy_cost_total = (Decimal(self.energy_cost_per_unit) * Decimal(charged_energy)) / Decimal(1000)
        time_cost_total = (Decimal(self.time_cost_per_unit) * Decimal(duration)) / Decimal(3600)
        transaction_cost_total = Decimal(self.transaction_cost)
        overall_cost = (energy_cost_total + time_cost_total + transaction_cost_total)
        return {
            "overall": float(overall_cost.quantize(Decimal('.01'))),
            "components": {
                "energy": float(energy_cost_total.quantize(Decimal('.001'))),
                "time": float(time_cost_total.quantize(Decimal('.001'))),
                "transaction": float(transaction_cost_total.quantize(Decimal('.001'))),
            }
        }


class Event(object):

    def __init__(self, time, meter):
        """Initialize class

        :param time: ISO 8601 timestamp
        :type time: string
        :param meter: meter at that time
        :type meter: int
        """
        self.time = parser.isoparse(time)
        self.meter = meter


class ChargeDetailRecord(object):

    def __init__(self, start, stop):
        """Initialize class

        :param start: Start event
        :type start: Event
        :param stop: Stop event
        :type stop: Event
        """
        self.start = start
        self.stop = stop

    @property
    def duration(self):
        """Get the duration of the charging process in seconds
        """
        return (self.stop.time - self.start.time).total_seconds()

    @property
    def charged_energy(self):
        """Get the overall delivered energy in Wh
        """
        return self.stop.meter - self.start.meter


class TransactionApplier(object):

    def rating(self, rate_dict, cdr_dict):
        """Apply the rate to the cdr

        :param rate_dict: The rate which needs to be applied
        :type rate_dict: dict
        :param cdr_dict: cdr
        :type cdr_dict: dict
        :return: Overall cost and cost of the single components
        :rtype: dict
        """
        rate = Rate(**rate_dict)

        start = Event(cdr_dict["timestampStart"], cdr_dict["meterStart"])
        stop = Event(cdr_dict["timestampStop"], cdr_dict["meterStop"])
        cdr = ChargeDetailRecord(start, stop)

        return rate.calculate(cdr.duration, cdr.charged_energy)
