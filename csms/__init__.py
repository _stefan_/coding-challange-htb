
from flask import Flask
from csms.core import TransactionApplier

# Create Flask application
app = Flask(__name__)

# Create Transaction Applier
transactions_applier = TransactionApplier()

import csms.api
