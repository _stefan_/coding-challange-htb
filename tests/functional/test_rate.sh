#!/usr/bin/env bash

# Example API call for the rate endpoint
curl -X POST "http://127.0.0.1:5000/rate" \
    -H "Content-Type: application/json" \
    -d '{ "rate": { "energy": 0.3, "time": 2, "transaction": 1 }, "cdr": { "meterStart": 1204307, "timestampStart": "2021-04-05T10:04:00Z", "meterStop": 1215230, "timestampStop": "2021-04-05T11:27:00Z" } }'
