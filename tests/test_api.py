
from pytest import approx
from csms import app


class TestRate:

    def test_success(self):
        """Test the /rate endpoint with a given example for input and output
        """

        parameters = {
            "rate": {
                "energy": 0.3,
                "time": 2,
                "transaction": 1,
            },
            "cdr": {
                "meterStart": 1204307,
                "timestampStart": "2021-04-05T10:04:00Z",
                "meterStop": 1215230,
                "timestampStop": "2021-04-05T11:27:00Z",
            }
        }
        response = app.test_client().post("/rate", json=parameters)

        assert response.status_code == 200
        r = response.get_json()
        assert r["overall"] == approx(7.04, abs=0.01)
        assert r["components"]["energy"] == approx(3.277, abs=0.001)
        assert r["components"]["time"] == approx(2.767, abs=0.001)
        assert r["components"]["transaction"] == approx(1, abs=0.001)

    def test_wrong_method(self):
        """Use a wrong method for the /rate endpoint
        """

        response = app.test_client().get("/rate")
        assert response.status_code == 405

        response = app.test_client().put("/rate")
        assert response.status_code == 405

        response = app.test_client().delete("/rate")
        assert response.status_code == 405

    def test_wrong_parameters(self):
        """Pass wrong parameters to the /rate entpoint
        """

        # Missing parameters
        response = app.test_client().post("/rate")
        assert response.status_code == 400

        # Missing rate
        parameters = {
            "cdr": {
                "meterStart": 1204307,
                "timestampStart": 1617617040,
                "meterStop": 1215230,
                "timestampStop": 1617622020,
            }
        }
        response = app.test_client().post("/rate", json=parameters)
        assert response.status_code == 422

        # Missing cdr
        parameters = {
            "rate": {
                "energy": 0.3,
                "time": 2,
                "transaction": 1,
            }
        }
        response = app.test_client().post("/rate", json=parameters)
        assert response.status_code == 422

        # Wrong timestamp format
        parameters = {
            "rate": {
                "energy": 0.3,
                "time": 2,
                "transaction": 1,
            },
            "cdr": {
                "meterStart": 1204307,
                "timestampStart": 1617617040,
                "meterStop": 1215230,
                "timestampStop": 1617622020,
            }
        }
        response = app.test_client().post("/rate", json=parameters)
        assert response.status_code == 422
