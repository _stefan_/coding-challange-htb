
import datetime
from dateutil.tz import tzutc
from pytest import approx
from csms.core import (
    ChargeDetailRecord,
    Event,
    Rate,
    TransactionApplier,
)

class TestCore:

    def test_event(self):
        """Test the time convertion of the event
        """
        iso_timestamp = "2022-08-01T12:00:00Z"
        charged_energy = 10000
        e = Event(iso_timestamp, charged_energy)

        assert e.time == datetime.datetime(2022, 8, 1, 12, 0, tzinfo=tzutc())

    def test_charge_detail_record(self):
        """Test the duration and the amount of energy which got charged
        """

        start = Event(time="2022-08-01T12:00:00Z", meter=4000)
        stop = Event(time="2022-08-01T13:00:00Z", meter=10000)
        cdr = ChargeDetailRecord(start, stop)

        assert cdr.duration == 3600
        assert cdr.charged_energy == 6000

    def test_rate(self):
        """Test the calculation of the overall cost and the cost of the single components
        """

        # Rate
        energy = 0.5
        time = 2
        transaction = 3

        # Charge details
        duration = 3500
        charged_energy = 9834

        rate = Rate(energy, time, transaction)
        r = rate.calculate(duration, charged_energy)

        assert r["overall"] == approx(9.86, abs=0.01)
        assert r["components"]["energy"] == approx(4.917, abs=0.001)
        assert r["components"]["time"] == approx(1.944, abs=0.001)
        assert r["components"]["transaction"] == approx(3, abs=0.001)

    def test_transaction_applier(self):
        """Test the whole transaction where the rate gets applied to the cdr
        """

        rate = {
            "energy": 0.2,
            "time": 1,
            "transaction": 4,
        }
        cdr = {
            "meterStart": 4285,
            "timestampStart": "2022-08-01T12:00:00Z",
            "meterStop": 10000,
            "timestampStop": "2022-08-01T12:59:00Z",
        }

        transactions_applier = TransactionApplier()
        r = transactions_applier.rating(rate, cdr)

        assert r["overall"] == approx(6.13, abs=0.01)
        assert r["components"]["energy"] == approx(1.143, abs=0.001)
        assert r["components"]["time"] == approx(0.983, abs=0.001)
        assert r["components"]["transaction"] == approx(4, abs=0.001)
